//
// Created by knthanh on 09/08/2017.
//

#ifndef ANNOTATION_TOOLS_INPUTCALLBACK_H
#define ANNOTATION_TOOLS_INPUTCALLBACK_H

#include <string>

class InputCallback
{
protected:
    std::string _windowName;
    int _prevX, _prevY;
    static void MouseCallBack(int event, int x, int y, int flag, void *callback);
public:
    enum MouseButton
    {
        LEFT_BUTTON,
        RIGHT_BUTTON,
        MIDDLE_BUTTON,
    };
    InputCallback();
    InputCallback(std::string windowName);
    void SetMouseCallback(std::string windowName);
    virtual void OnMouseMove(int prevX, int prevY, int x, int y, int flags);
    virtual void OnMButtonDown(MouseButton button, int x, int y, int flags);
    virtual void OnMButtonUp(MouseButton button, int x, int y, int flags);
    virtual void OnMButtonDBCLK(MouseButton button, int x, int y, int flags);
    virtual void OnMWheel(bool isHorizontal, int x, int y, int flags);
};


#endif //ANNOTATION_TOOLS_INPUTCALLBACK_H
