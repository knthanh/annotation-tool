//
// Created by knthanh on 07/08/2017.
//

#ifndef ANNOTATION_TOOLS_UTILS_H
#define ANNOTATION_TOOLS_UTILS_H

#include <string>
#include <boost/filesystem.hpp>
#include "BoundingBox.h"

namespace fs = boost::filesystem;

std::string ToString(BBVector bbvector);
std::vector<std::string> ListFiles(fs::path dir, std::string ext = "");

#endif //ANNOTATION_TOOLS_UTILS_H
