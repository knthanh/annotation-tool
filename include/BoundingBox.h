//
// Created by knthanh on 07/08/2017.
//

#ifndef ANNOTATION_TOOLS_BOUNDINGBOX_H
#define ANNOTATION_TOOLS_BOUNDINGBOX_H

#include <opencv2/core/types.hpp>
#include <vector>


class BoundingBox
{
protected:
    cv::Rect _box;
    unsigned int _id;

public:
    enum Side
    {
        TOP_LEFT,
        TOP,
        TOP_RIGHT,
        RIGHT,
        BOTTOM_RIGHT,
        BOTTOM,
        BOTTOM_LEFT,
        LEFT,
    };

    BoundingBox(unsigned int id);
    BoundingBox(unsigned int id, cv::Rect box);

    void Translate(cv::Point dir = cv::Point(0, 0));
    void Scale(double wFactor = 1, double hFactor = 1);
    void Drag(Side side, double wVal, double hVal);
    bool IsOverlap(BoundingBox box);

    inline
    unsigned int& ID()
    {
        return _id;
    }

    inline
    cv::Rect& Rect()
    {
        return _box;
    }

    bool operator==(BoundingBox& rhs);
};

typedef std::vector<BoundingBox> BBVector;

#endif //ANNOTATION_TOOLS_TRACKER_H
