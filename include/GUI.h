//
// Created by knthanh on 07/08/2017.
//

#ifndef ANNOTATION_TOOLS_GUI_H
#define ANNOTATION_TOOLS_GUI_H

#include <string>
#include <boost/program_options.hpp>
#include <opencv2/opencv.hpp>
#include <boost/filesystem.hpp>
#include "BoundingBox.h"
#include "InputCallback.h"

namespace po = boost::program_options;
namespace fs = boost::filesystem;

class GUI : public InputCallback
{
private:
    std::string _windowName,
                _helpWindow;

    fs::path _inputPath;
    int _curIdx;
    BoundingBox* _curBox;

    std::vector<BBVector> _bboxList;
    std::vector<cv::Mat> _frameList;

    po::variables_map _opt;
    cv::VideoCapture _vid;

    int _frameNum;
    int _curFrame;

    int _err;

    bool _isDrag;
    void Init();
    void DrawHelpWindows();
    int CheckIfClickBBox(int x, int y);
public:
    GUI(std::string windowName, po::variables_map options);
    void Run();
    void Save();
    void Load();

    void OnMouseMove(int prevX, int prevY, int x, int y, int flags) override;
    void OnMButtonDown(MouseButton button, int x, int y, int flags) override;
    void OnMButtonUp(MouseButton button, int x, int y, int flags) override;
};


#endif //ANNOTATION_TOOLS_GUI_H
