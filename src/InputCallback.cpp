//
// Created by knthanh on 09/08/2017.
//

#include <opencv/cv.hpp>
#include "InputCallback.h"

void InputCallback::MouseCallBack(int event, int x, int y, int flag, void *callback)
{
    InputCallback* inputCB = (InputCallback*) callback;

    if(event < 0)
    {
        printf("Error : No match event %i.\n", event);
        return;
    }

    if(event == cv::MouseEventTypes::EVENT_MOUSEMOVE)
    {
        if(inputCB->_prevX == -1 || inputCB->_prevY == -1)
        {
            inputCB->_prevX = x;
            inputCB->_prevY = y;
        }

        inputCB->OnMouseMove(inputCB->_prevX, inputCB->_prevY, x, y, flag);

        inputCB->_prevX = x;
        inputCB->_prevY = y;
    }
    else if(event <= cv::MouseEventTypes::EVENT_MBUTTONDOWN)
    {
        if(event == cv::MouseEventTypes::EVENT_LBUTTONDOWN)
        {
            inputCB->OnMButtonDown(LEFT_BUTTON, x, y, flag);
        }
        else if (event == cv::MouseEventTypes::EVENT_RBUTTONDOWN)
        {
            inputCB->OnMButtonDown(RIGHT_BUTTON, x, y, flag);
        }
        else
        {
            inputCB->OnMButtonDown(MIDDLE_BUTTON, x, y, flag);
        }
    }
    else if (event <= cv::MouseEventTypes::EVENT_MBUTTONUP)
    {
        if(event == cv::MouseEventTypes::EVENT_LBUTTONUP)
        {
            inputCB->OnMButtonUp(LEFT_BUTTON, x, y, flag);
        }
        else if (event == cv::MouseEventTypes::EVENT_RBUTTONUP)
        {
            inputCB->OnMButtonUp(RIGHT_BUTTON, x, y, flag);
        }
        else
        {
            inputCB->OnMButtonUp(MIDDLE_BUTTON, x, y, flag);
        }
    }
    else if (event <= cv::MouseEventTypes::EVENT_MBUTTONDBLCLK)
    {
        if(event == cv::MouseEventTypes::EVENT_LBUTTONDBLCLK)
        {
            inputCB->OnMButtonDBCLK(LEFT_BUTTON, x, y, flag);
        }
        else if (event == cv::MouseEventTypes::EVENT_RBUTTONDBLCLK)
        {
            inputCB->OnMButtonDBCLK(RIGHT_BUTTON, x, y, flag);
        }
        else
        {
            inputCB->OnMButtonDBCLK(MIDDLE_BUTTON, x, y, flag);
        }
    }
    else if (event <= cv::MouseEventTypes::EVENT_MOUSEHWHEEL)
    {
        if(event == cv::MouseEventTypes::EVENT_MOUSEWHEEL)
        {
            inputCB->OnMWheel(false, x, y, flag);
        }
        else
        {
            inputCB->OnMWheel(true, x, y, flag);
        }
    }
    else
    {
        printf("Error : No match event %i.\n", event);
    }
}

InputCallback::InputCallback():
_prevX(-1),
_prevY(-1)
{}

InputCallback::InputCallback(std::string windowName) :
InputCallback()
{
    SetMouseCallback(_windowName);
}

void InputCallback::SetMouseCallback(std::string windowName)
{
    _windowName = windowName;
    cv::setMouseCallback(_windowName, MouseCallBack, this);
}

void InputCallback::OnMouseMove(int prevX, int prevY, int x, int y, int flags)
{
//    printf("Default Function:\n"
//                   "\tMouse move:\n"
//                   "\t\tPrev: %i %i\n"
//                   "\t\tCur : %i %i\n", prevX, prevY, x, y);
}

void InputCallback::OnMButtonDown(InputCallback::MouseButton button, int x, int y, int flags)
{
//    printf("Default Function:\n"
//                   "\tMouse button down:\n"
//                   "\t\tButton: %s\n"
//                   "\t\tPos : %i %i\n", button == LEFT_BUTTON? "Left mouse button" :
//                                        button == RIGHT_BUTTON ? "Right mouse button" :
//                                        "Middle mouse button", x, y);
}

void InputCallback::OnMButtonUp(InputCallback::MouseButton button, int x, int y, int flags)
{
//    printf("Default Function:\n"
//                   "\tMouse button up:\n"
//                   "\t\tButton: %s\n"
//                   "\t\tPos : %i %i\n", button == LEFT_BUTTON? "Left mouse button" :
//                                        button == RIGHT_BUTTON ? "Right mouse button" :
//                                        "Middle mouse button", x, y);
}

void InputCallback::OnMButtonDBCLK(InputCallback::MouseButton button, int x, int y, int flags)
{
//    printf("Default Function:\n"
//                   "\tMouse button double click:\n"
//                   "\t\tButton: %s\n"
//                   "\t\tPos : %i %i\n", button == LEFT_BUTTON? "Left mouse button" :
//                                        button == RIGHT_BUTTON ? "Right mouse button" :
//                                        "Middle mouse button", x, y);
}

void InputCallback::OnMWheel(bool isHorizontal, int x, int y, int flags)
{
//    printf("Default Function:\n"
//                   "\tMouse wheel:\n"
//                   "\t\tIs horizontal: %s\n"
//                   "\t\tVal : %i %i\n", isHorizontal? "TRUE" : "FALSE"
//                                      , x, y);
}
