//
// Created by knthanh on 07/08/2017.
//

#include "utils.h"


std::string ToString(BBVector bbvector)
{
    std::string result = "";
    for(auto& bbox : bbvector)
    {
        cv::Rect box = bbox.Rect();

        result += std::to_string(box.x) + " " + std::to_string(box.y) + " " +
                  std::to_string(box.width) + " " + std::to_string(box.height) + " " +
                  std::to_string(bbox.ID()) + " ";
    }

    return result;
}

std::vector<std::string> ListFiles(fs::path dir, std::string ext)
{
    std::vector<std::string> fileList;
    fs::directory_iterator endIt;
    bool hasExt = false;
    if(ext.size() > 0)
    {
        hasExt = true;
    }
    for(fs::directory_iterator it(dir); it != endIt; ++it)
    {
        if(fs::is_regular_file(it->path()) && fs::file_size(it->path()) > 0)
        {
            if(hasExt && it->path().extension() == ext)
            {
                fileList.push_back(it->path().string());
            }
        }
    }
    std::sort(fileList.begin(), fileList.end());
    return fileList;
}