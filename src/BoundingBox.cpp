//
// Created by knthanh on 07/08/2017.
//

#include "BoundingBox.h"

BoundingBox::BoundingBox(unsigned int id):
BoundingBox(id, cv::Rect())
{

}

BoundingBox::BoundingBox(unsigned int id, cv::Rect box)
:
_id(id),
_box(box)
{

}

void BoundingBox::Translate(cv::Point dir)
{
    _box.x += dir.x;
    _box.y += dir.y;
}

void BoundingBox::Scale(double wFactor, double hFactor)
{
    _box.width *= wFactor;
    _box.height *= hFactor;
}

void BoundingBox::Drag(Side side, double wVal, double hVal)
{
    if(side == TOP || side == TOP_LEFT || side == TOP_RIGHT)
    {
        _box.y += hVal;
        _box.height -= hVal;
    }

    if(side == BOTTOM || side == BOTTOM_LEFT || side == BOTTOM_RIGHT)
    {
        _box.height += hVal;
    }

    if(side == TOP_LEFT || side == BOTTOM_LEFT)
    {
        _box.x += wVal;
        _box.width -= wVal;
    }

    if(side == TOP_RIGHT || side == BOTTOM_RIGHT)
    {
        _box.width += wVal;
    }
}

bool BoundingBox::IsOverlap(BoundingBox box)
{
    cv::Point center = (_box.tl() + _box.br()) / 2;
    cv::Point bCenter = (box._box.tl() + box._box.br()) / 2;
    return _box.contains(bCenter) || box._box.contains(center);
}

bool BoundingBox::operator==(BoundingBox& rhs)
{
    return this->_id == rhs._id || this->IsOverlap(rhs);
}