//
// Created by knthanh on 07/08/2017.
//

#include <iostream>
#include <fstream>

#include "GUI.h"
#include "utils.h"

GUI::GUI(std::string windowName, po::variables_map options) :
_windowName(std::move(windowName)),
_opt(std::move(options))
{
    _helpWindow = "Help";
    cv::namedWindow(_windowName, cv::WINDOW_AUTOSIZE);
//    cv::namedWindow(_helpWindow);
    this->SetMouseCallback(_windowName);
    _frameNum = 0;
    _err = 0;
    _curFrame = -1;
    _curIdx = -1;
    _curBox = nullptr;
    _isDrag = false;
}

void GUI::Run()
{
    Init();

    assert(_err >= 0);

    for(_curFrame = 0; _curFrame < _frameNum; )
    {
        cv::Mat curFrame = _frameList[_curFrame].clone();
        for (auto &curBox : _bboxList[_curFrame])
        {
            cv::rectangle(curFrame, curBox.Rect(), cv::Scalar(0,255,0));
            cv::putText(curFrame, std::to_string(curBox.ID()),
                        cv::Point(curBox.Rect().x,
                                  curBox.Rect().y + curBox.Rect().height + 30),
                        cv::FONT_HERSHEY_COMPLEX, 0.9, cv::Scalar(0,255,0));
        }
        cv::imshow(_windowName, curFrame);

        int key = cv::waitKey(33);

        if(key == 'q') break;

        switch (key)
        {
            case 'w':
            case 'W':
                if(_curBox != nullptr)
                    ++_curBox->ID();
                break;
            case 's':
            case 'S':
                if(_curBox != nullptr)
                    --_curBox->ID();
                break;
            case 'a':
            case 'A':
                --_curFrame;
                if(_curFrame < 0)
                    _curFrame = 0;
                break;
            case 'd':
            case 'D':
                ++_curFrame;
                break;
            default:break;
        }
    }
    Save();
}

void GUI::OnMouseMove(int prevX, int prevY, int x, int y, int flags)
{
    if(((flags & cv::MouseEventFlags::EVENT_FLAG_LBUTTON) != 0) && _curBox != nullptr)
    {
        if(_isDrag)
        {
            _curBox->Drag(BoundingBox::Side::BOTTOM_RIGHT, x - prevX, y - prevY);
        }
        else
        {
            _curBox->Translate(cv::Point(x- prevX, y - prevY));
        }
    }
}

void GUI::OnMButtonDown(InputCallback::MouseButton button, int x, int y, int flags)
{
    if(_curFrame < 0) return;
    _curIdx = CheckIfClickBBox(x,y);

    switch (button)
    {
        case LEFT_BUTTON:
        {
            if (_curIdx < 0)
            {
                _bboxList[_curFrame].emplace_back(BoundingBox(0, cv::Rect(x, y, 1, 1)));
                _curBox = &_bboxList[_curFrame].back();
                _isDrag = true;
                _curIdx = static_cast<int>(_bboxList.size() - 1);
            }
            else
            {
                _curBox = &_bboxList[_curFrame][_curIdx];
            }
        }
            break;
        case RIGHT_BUTTON:break;
        case MIDDLE_BUTTON:
        {
            if(_curIdx >= 0)
            {
                _bboxList[_curFrame].erase(_bboxList[_curFrame].begin() + _curIdx);
                _curBox = nullptr;
                _curIdx = -1;
            }
        }
            break;
    }
}

void GUI::OnMButtonUp(InputCallback::MouseButton button, int x, int y, int flags)
{
    switch (button)
    {
        case LEFT_BUTTON:
            if(_curIdx < 0) break;

            if(_curBox->Rect().width * _curBox->Rect().height <= 1 ||
                    _curBox->Rect().width <= 20 || _curBox->Rect().height <= 20)
            {
                printf("Delete id %i\n", _curIdx);
                _bboxList[_curFrame].erase(_bboxList[_curFrame].begin() + _curIdx);
                _curBox = nullptr;
                _curIdx = -1;
            }
            break;
        case RIGHT_BUTTON:break;
        case MIDDLE_BUTTON:break;
    }
    _isDrag = false;
}

void GUI::Init()
{
    if(_opt.count("video") > 0)
    {
        printf("Use video instead of images.\n");
        _inputPath = _opt["video"].as<fs::path>();
        _vid = cv::VideoCapture(_inputPath.string());
        _inputPath = _inputPath.parent_path();

        if(!_vid.isOpened())
        {
            printf("Error: Cannot open file.\n");
            _err = -1;
            return;
        }

        _frameNum = int(_vid.get(cv::CAP_PROP_FRAME_COUNT));
        _frameList.reserve(static_cast<unsigned long>(_frameNum));
        _bboxList.reserve(static_cast<unsigned long>(_frameNum));

        try
        {
            cv::Mat frame;
            for(int i = 0; i < _frameNum; ++i)
            {
                _vid.read(frame);
                if(!frame.empty())
                {
                    _frameList.push_back(frame.clone());
                }
            }
            _frameNum = _frameList.size();
            _bboxList.resize(_frameNum);
        }
        catch (cv::Exception e)
        {
            printf("Error: %s\n",e.what());
        }
    }
    else if (_opt.count("input-dir"))
    {
        _inputPath = _opt["input-dir"].as<fs::path>();
        auto fileList = ListFiles(_opt["input-dir"].as<fs::path>(), ".jpg");

        _frameNum = static_cast<int>(fileList.size());
        _frameList.resize(static_cast<unsigned long>(_frameNum));
        _bboxList.resize(static_cast<unsigned long>(_frameNum));

        try
        {
            cv::Mat frame;
            for(int i = 0; i < _frameNum; ++i)
            {
                frame = cv::imread(fileList[i]);
                if(frame.empty()) break;
                _frameList[i] = frame;
            }
        }catch (cv::Exception e)
        {
            printf("Error: %s\n",e.what());
        }
    }
    else
    {
        printf("Please input input directory or video file name.\n");
        return;
    }
}

void GUI::DrawHelpWindows()
{

}

int GUI::CheckIfClickBBox(int x, int y)
{
    cv::Point curPos(x,y);
    for(int i = static_cast<int>(_bboxList[_curFrame].size() - 1); i >= 0; --i)
    {
        if(_bboxList[_curFrame][i].Rect().contains(curPos))
        {
            return i;
        }
    }
    return -1;
}

void GUI::Save()
{
    std::string fileName = (fs::canonical(_inputPath) / fs::path(_inputPath.parent_path().string() + "_gt.txt")).string();
    if(_opt.count("out"))
    {
        fs::path outFile = _opt["out"].as<fs::path>();
        fileName = (fs::canonical(outFile)).string();
    }

    std::ofstream out(fileName);
    if(out.is_open())
    {
        printf("Save to %s\n", fileName.c_str());
        for(auto&bboxVector : _bboxList)
        {
            out << ToString(bboxVector) << std::endl;
        }
    }
    else
    {
        printf("Cannot open file %s \n", fileName.c_str());
    }

}

void GUI::Load()
{
    std::string fileName = (fs::canonical(_inputPath) / fs::path(_inputPath.parent_path().string() + "_gt.txt")).string();
    std::ifstream in(fileName);
    if(in.is_open())
    {

    }
    else
    {
        printf("Cannot open file %s \n", fileName.c_str());
    }
}
