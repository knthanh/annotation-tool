import os
import sys
import itertools
import argparse
import pylab

class ModelParameter:
    """ Class to encapsulate model parameter """
    """ For example: {'-c': 0} """
    def __init__(self, flag, value):
        self.flag = flag
        self.value = value

    def __eq__(self, other):
        return self.flag == other.flag and self.value == other.value

    def __repr__(self):
        return '{%s: %s}' % (self.flag, self.value)

    def to_command_arg(self):
        return '%s %s' % (self.flag, self.value)


data_file_names = ['test01', 'test02', 'test03', 'test04', 'test05', 'goldsun01', 'goldsun02', 'goldsun03']
detect_flags = ['-c', '-e', '-f', '-xp', '-yp']
recog_flags = ['-s', '-p', '-m']


def create_range(values):
    """ Create a range of values from 3 number (start, end, step) """
    values = list(map(float, values))
    return pylab.frange(values[0], values[1], values[2]).tolist()

def read_config_file(config_path):
    """ Read the configuration file and build a map of parameters to flags """
    parameters = {}
    with open(config_path) as config_file:
        for line in config_file:
            values = line.strip().split()
            try:
                parameters[values[0]] = [ModelParameter(values[0], value) for value in create_range(values[1:])]
            except ValueError:
                parameters[values[0]] = [ModelParameter(values[0], value) for value in values[1:]]
    print('Read %s' % (config_path))
    return parameters

def build_parameter_list(parameters):
    """ Convert a map of parameters to a list so we can find the product """
    parameter_list = []
    for flag in parameters.keys():
        parameter_list.append(parameters[flag])
    return parameter_list

def execute_model(combination, model, config_path, data_path, data_file_name, mode):
    """ Execute the model with one combination of parameters for one data file """
    flags = detect_flags if mode == 'detect' else recog_flags
    parameters = [parameter for parameter in combination if parameter.flag in flags]
    command_template = model + ' ' + ' '.join([parameter.to_command_arg() for parameter in parameters])
    output_path_template = '_' + '_'.join([str(parameter.value) for parameter in parameters]) + '_' + mode

    gt_path = data_file_name + '_gt.txt'
    command = '%s -i %s/%s.txt' % (command_template, data_path, data_file_name)
    predict_path = data_file_name + output_path_template + '_predict.txt'
    result_path = data_file_name + output_path_template + '_result.txt'

    print('\nRun %s' % (command))
    os.system(command)
    os.system('python calc_accuracy.py -r %s -g %s -o %s' % (predict_path, gt_path, result_path))

def main(detector, recognizer, config_path, data_path):
    parameters = read_config_file(config_path)
    parameter_list = build_parameter_list(parameters);

    for combination in list(itertools.product(*parameter_list)):
        for data_file_name in data_file_names:
            execute_model(combination, detector, config_path, data_path, data_file_name, 'detect')
            execute_model(combination, recognizer, config_path, data_path, data_file_name, 'recog')
    print('\nFinished')


if __name__ == '__main__':
    parser = argparse.ArgumentParser('Cross Validation', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-detector', '--detector', help='Model for face detection', required=True)
    parser.add_argument('-recognizer', '--recognizer', help='Model for face recognizer', required=True)
    parser.add_argument('-config', '--config', help='Path of configuration file', required=True)
    parser.add_argument('-data', '--data', help='Path to data folder', required=True)
    args = parser.parse_args()
    main(args.detector, args.recognizer, args.config, args.data)