import os
import sys
import argparse


class BoundingBox:
    """ The rectangle box around the face """
    def __init__(self):
        __init__(self, 0, 0, 0 ,0)

    def __init__(self, left, top, width, height):
        self.left = left
        self.top = top
        self.width = width
        self.height = height

    def overlap(self, box):
        return (self.left + self.width/2 >= box.left and self.left + self.width/2 <= box.left + box.width/2) and \
            (self.top + self.height/2 >= box.top and self.top + self.height/2 <= box.top + box.height/2)

    def __eq__(self, other):
        return (self.left == other.left) and (self.top == other.top) and \
            (self.width == other.width) and (self.height == other.height)

    def __str__(self):
        return '(%d,%d,%d,%d)' % (self.left, self.top, self.width, self.height)

class Result:
    """ The result represented by 5 numbers in the file """
    def __init__(self, id, box):
        self.id = id
        self.box = box

    def match(self, result, identification_mode):
        return (self.id == result.id or not identification_mode) and self.box.overlap(result.box)

    def __eq__(self, other):
        return self.id == other.id and self.box == other.box

    def __str__(self):
        return '{id: %d, box: %s}' % (self.id, self.box)

def parse_result(line):
    """ For each line in the file, read 5 numbers to build a Result """
    results = []
    line = line.split()
    if len(line) <= 0 or len(line) % 5 != 0:
        raise ValueError('Invalid file format')
    line = map(int, line)
    for left, top, width, height, id in zip(*[iter(line)]*5):
        results.append(Result(id, BoundingBox(left, top, width, height)))
    return results

def update_confusion_matrix(confusion_matrix, predicted, actual, identification_mode):
    """ Update the confusion matrix based on predicted results and actual results """
    for groundtruth in actual:
        matched = False
        for prediction in predicted:
            if prediction.id == groundtruth.id or not identification_mode:
                if prediction.match(groundtruth, identification_mode):
                    confusion_matrix[0][0] += 1
                    matched = True
                    break
                elif identification_mode:
                    confusion_matrix[0][1] += 1
                    matched = True
                    break
        if not matched:
            confusion_matrix[1][0] += 1
    if not identification_mode:
        confusion_matrix[0][1] += len(predicted) - confusion_matrix[0][0]
    return confusion_matrix

def write_matrix(matrix, path):
    """ Write the confusion matrix into file """
    with open(path, 'w+') as file:
        file.write('%d %d\n%d %d' % (matrix[0][0], matrix[0][1], matrix[1][0], matrix[1][1]))

def main(identification_mode, result_path, gt_path, output_path):
    confusion_matrix = [[0, 0], [0, 0]]
    with open(result_path) as result_file, open(gt_path) as gt_file:
        for result_line, gt_line in zip(result_file, gt_file):
            predicted = parse_result(result_line.strip())
            actual = parse_result(gt_line.strip())
            update_confusion_matrix(confusion_matrix, predicted, actual, identification_mode)
    print('Confusion matrix: %s' % (confusion_matrix))
    write_matrix(confusion_matrix, output_path)
    return

if __name__ == '__main__':
    parser = argparse.ArgumentParser('Test', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-r', '--result-path', help='Path of result file', required=True)
    parser.add_argument('-g', '--gt-path', help='Path of ground truth file', required=True)
    parser.add_argument('-o', '--output-path', help='Path of output file', required=True)
    parser.add_argument('-id', '--identification', help='Test mode', action='store_true')
    args = parser.parse_args()
    main(args.identification, args.result_path, args.gt_path, args.output_path)