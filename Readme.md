Installation:
The program require the following package:
- boost 1.64: filesystem, system, program options, timer
- opencv >= 3.2

Which could be build using the command:

./build_3rdparty.sh

To build the tool

./build_tool.sh

To clean all:

./clean_all.sh

You don't need to build 3rdparty if it is already built or there are corruption in the lib folders.
Simply use build_tool.sh for every update.

To use the tools:

./annotation_tools -i input_directories

To use the program:
a : move to prev frame
d : move to next frame
w : increase Box ID
s : decrease Box ID

Left-click: select or draw new box
Middle-click: remove underlying box