#include <iostream>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <GUI.h>

namespace po = boost::program_options;
namespace fs = boost::filesystem;

int main(int argc, const char* const * argv)
{
    po::options_description desc("Command list");
    desc.add_options()
            ("input-dir,i", po::value<fs::path>(), "Input folder")
            ("video,v", po::value<fs::path>(), "Path to video (optional)")
            ("out,o", po::value<fs::path>(), "output file")
            ("help,h","TODO");

    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv)
            .options(desc)
            .style(po::command_line_style::unix_style |
                   po::command_line_style::allow_long_disguise )
            .run(),
            vm);

    GUI gui("Windows", vm);
    gui.Run();
    return 0;
}