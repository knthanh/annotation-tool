cd 3rdparty
tar -xzvf 3rdparty.tar.gz

sudo apt-get update
sudo apt-get install qt5-default

cd boost_1_64_0/
./bootstrap.sh --with-libraries=program_options,filesystem,timer,system
sudo ./b2 -j 4 --prefix=`pwd`/build install
sudo mv build/lib/ ../../
cd ..

cd opencv-3.3.0/
mkdir build
cd build 
cmake -DWITH_QT=TRUE -DBUILD_DOCS=FALSE .. 
make -j 4
sudo make install